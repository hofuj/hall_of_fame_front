export interface ISortField {
  asc: boolean
  name: "updatedAt" | "voteCount"
}
