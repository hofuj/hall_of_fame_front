import { ICategory } from "./category"

export interface ITeamMember {
  id: string
  name: string
  surname: string
  personalWebsiteUrl?: string
}

export interface ITeam {
  id: string
  name?: string
  members: ITeamMember[]
}

export interface IProject {
  id: string
  name: string
  description: string
  team: ITeam
  category: ICategory
  codeUrl?: string
  websiteUrl?: string
  images: IImage[]
  updatedAt: Date
  voteCount: number
  currentUserVote: number
}

export interface IImage {
  url: string
  id: number
}
