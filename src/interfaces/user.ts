export interface IUser {
  name: string
  surname: string
  personalWebsiteUrl?: string
}
