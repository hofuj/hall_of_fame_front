import React from "react"
import GlobalStyle from "./App.style"

import { Redirect } from "react-router"

import Projects from "./components/Projects/Projects"
import Project from "./components/Project/Project"
import AdminProjects from "./components/Admin/AdminProjects/AdminProjects"
import AddProjectAdmin from "./components/AddProjectForm/AddProjectAdmin"
import AddProjectRequest from "./components/AddProjectForm/AddProjectRequest"
import Login from "./components/Admin/Login/Login"
import Activate from "./components/Admin/Activate/Activate"
import Requested from "./components/Admin/Requested/Requested"
import AdminProject from "./components/Admin/AdminProject/AdminProject"
import AdminProjectRequested from "./components/Admin/AdminProjectRequested/AdminProjectRequested"

import { BrowserRouter as Router, Switch, Route } from "react-router-dom"
import EditProject from "./components/Admin/EditProject/EditProject"
import { ToastContainer } from "react-toastify"
import "react-toastify/dist/ReactToastify.css"

function App() {
  return (
    <Router>
      <GlobalStyle />
      <Switch>
        <Route exact={true} path="/projects">
          <Redirect to="/" />
        </Route>

        <Route exact={true} path="/" component={Projects} />
        <Route exact={true} path="/projects/:page" component={Projects} />

        <Route exact={true} path="/project/new" component={AddProjectRequest} />
        <Route exact={true} path="/project/:projectId" component={Project} />

        <Route exact={true} path="/admin">
          <Redirect to="/admin/projects" />
        </Route>

        <Route exact={true} path="/admin/projects" component={AdminProjects} />
        <Route exact={true} path="/admin/projects/:page" component={AdminProjects} />

        <Route exact={true} path="/admin/login" component={Login} />
        <Route exact={true} path="/admin/activate" component={Activate} />

        <Route exact={true} path="/admin/requested" component={Requested} />
        <Route exact={true} path="/admin/requested/:page" component={Requested} />

        <Route exact={true} path="/admin/project/new" component={AddProjectAdmin} />

        <Route exact={true} path="/admin/project/:projectId" component={AdminProject} />
        <Route
          exact={true}
          path="/admin/project/requested/:projectId"
          component={AdminProjectRequested}
        />

        <Route exact={true} path="/admin/project/edit/:projectId" component={EditProject} />
      </Switch>
      <ToastContainer style={{ fontSize: "1.2rem" }} />
    </Router>
  )
}

export default App
