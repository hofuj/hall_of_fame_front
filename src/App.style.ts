import { createGlobalStyle } from "styled-components"

export default createGlobalStyle`
    * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }

    html {
        font-size: 10px;

        @media(max-width: 1200px){
            font-size: 9px;
        }

        @media(max-width: 900px){
            font-size: 8px;
        }

        @media(max-width: 600px){
            font-size: 7px;
        }
    }

    body {
        background: #EBEBEB;
        font-family: 'Roboto', sans-serif;
    }

    a {
        color: inherit;
        text-decoration: none;
    }
`
