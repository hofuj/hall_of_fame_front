import React, { useState } from "react"
import { RouteComponentProps } from "react-router"

import Fetcher from "../../../common/Fetcher"
import getConfig from "../../../common/config"

import { ISortField } from "../../../interfaces/sort"

import { ShowFilterButton, FilterBarContainer } from "./AdminProjects.style"

import WithSideNav from "../../SideNav/WithSideNav"
import WithAuth from "../WithAuth/WithAuth"

import FilterBar from "../../Projects/FilterBar/FilterBar"
import ModalTemplate from "../../ModalTemplate/ModalTemplate"
import ProjectsContainer from "../../Projects/ProjectsContainer/ProjectsContainer"

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faFilter } from "@fortawesome/free-solid-svg-icons"

interface IQueryParams {
  page?: string
}

interface IProps extends RouteComponentProps<IQueryParams> {}

const AdminProjects: React.FC<IProps> = ({ match }) => {
  const [categoryFilter, setCategoryFilter] = useState<string[]>([])
  const [sortField, setSortField] = useState<ISortField>({ asc: false, name: "updatedAt" })
  const [isFilterVisible, setFilterVisible] = useState<boolean>(false)
  const [isRmModalOpen, setRmModalOpen] = useState<boolean>(false)
  const [projectId, setProjectId] = useState<string>("")
  const [searchValue, setSearchValue] = useState<string>("")
  const [isLoading, setIsLoading] = useState<boolean>(false)

  const changeCategoryFilter = (newCategoryFilter: string[]) => {
    setCategoryFilter(newCategoryFilter)
  }

  const changeSortField = (newSortField: ISortField) => {
    setSortField(newSortField)
  }

  const updateFilterBar = () => {
    setFilterVisible((oldVal) => !oldVal)
  }

  const toggleRmModal = () => {
    setRmModalOpen((oldVal) => !oldVal)
  }

  const removeHandler = async (id: string) => {
    const fetcher = new Fetcher(getConfig().apiUrl)
    setIsLoading(true)
    try {
      await fetcher.deleteData(`/admin/project/${id}`)
      window.location.reload()
    } catch (e) {
      setIsLoading(false)
    }
  }

  const setPressedProjectId = (id: string) => {
    setProjectId(id)
  }

  const handleSearchChange = (e: any) => {
    setSearchValue(e.target.value)
  }

  return (
    <>
      <ModalTemplate
        message="Czy na pewno chcesz usunąć ten projekt?"
        acceptText="Usuń"
        cancelText="Anuluj"
        acceptFunction={removeHandler}
        cancelFunction={toggleRmModal}
        isOpen={isRmModalOpen}
        isLoading={isLoading}
        id={projectId}
      />

      <ProjectsContainer
        params={match.params}
        categoryFilter={categoryFilter}
        sortField={sortField}
        path="/project"
        pagePath="/admin/projects"
        isAdmin={true}
        toggleRmModal={toggleRmModal}
        setPressedId={setPressedProjectId}
        linkTo="/admin/project"
        searchValue={searchValue}
        isFullWidth={true}
      />

      <FilterBarContainer isFilterVisible={isFilterVisible}>
        <FilterBar
          changeCategoryFilter={changeCategoryFilter}
          changeSortField={changeSortField}
          sortField={sortField}
          isFullWidth={true}
          handleSearchChange={handleSearchChange}
          searchValue={searchValue}
        />
      </FilterBarContainer>

      <ShowFilterButton onClick={updateFilterBar}>
        <FontAwesomeIcon icon={faFilter} />
      </ShowFilterButton>
    </>
  )
}

export default WithAuth(WithSideNav(AdminProjects))
