import styled from "styled-components"

interface FilterBarContainerProps {
  isFilterVisible: boolean
}

export const ShowFilterButton = styled.div`
  background: #1d1f27;
  width: 5rem;
  height: 5rem;
  border-radius: 50%;
  position: fixed;
  top: 2rem;
  right: 4rem;
  display: flex;
  justify-content: center;
  align-items: center;
  color: white;
  font-size: 1.3rem;
  cursor: pointer;
  z-index: 100000000;
`

export const FilterBarContainer = styled.div`
  position: fixed;
  height: 100vh;
  width: 35rem;
  right: ${(props: FilterBarContainerProps) => (props.isFilterVisible ? "0" : "-35rem")};
  top: 0;
  z-index: 10000000;
  transition: all 0.5s;
  border-left: 3px solid #1d1f27;

  @media (max-width: 1100px) {
    width: 30rem;
    right: ${(props: FilterBarContainerProps) => (props.isFilterVisible ? "0" : "-30rem")};
  }

  @media (max-width: 800px) {
    width: 25rem;
    right: ${(props: FilterBarContainerProps) => (props.isFilterVisible ? "0" : "-25rem")};
  }
`
