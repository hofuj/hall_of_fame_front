import styled from "styled-components"

export const FormContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  height: 100vh;
`

export const Form = styled.form`
  width: 45%;
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
`

export const Input = styled.input`
  width: 100%;
  padding: 1.2rem;
  padding-left: 1.5rem;
  padding-top: 3rem;
  border: 2px solid #dee3de;
  border-radius: 5px;
  font-family: "Roboto", sans-serif;
  font-size: 1.3rem;
  background: #f7f7f7;
  margin: 0.5rem;

  &::placeholder {
    color: #b4b4b4;
  }
`

export const FormLabel = styled.label`
  position: absolute;
  left: 2.3rem;
  top: 1.7rem;
  font-weight: bold;
  font-size: 0.9rem;
`

export const Button = styled.button`
  padding: 1.3rem 6rem;
  border: none;
  font-family: inherit;
  font-size: 1.2rem;
  border-radius: 8px;
  background: #1d1f27;
  margin-top: 1rem;
  color: white;
`
