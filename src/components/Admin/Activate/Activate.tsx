import React, { useState } from "react"

import Fetcher from "../../../common/Fetcher"
import getConfig from "../../../common/config"

import WithSideNav from "../../SideNav/WithSideNav"
import WithAuth from "../WithAuth/WithAuth"

import { FormContainer, Form, FormLabel, Input, Button } from "./Active.style"

interface SuccessActivate {
  userId: number
  token: string
  isActive: boolean
}

const Activate = () => {
  const [password, setPassword] = useState<string>("")
  const [rtpPassword, setRtpPasswrod] = useState<string>("")

  const activateHandler = async (e: any) => {
    e.preventDefault()
    const fetcher = new Fetcher(getConfig().apiUrl)
    try {
      const response = await fetcher.postData<SuccessActivate>("/admin/activate", {
        password,
        retypedPassword: rtpPassword,
      })
      localStorage.setItem("token", response.token)
      window.location.replace("/admin/projects")
    } catch (e) {
      console.log(e)
    }
  }

  return (
    <FormContainer>
      <Form onSubmit={activateHandler}>
        <div style={{ position: "relative", width: "100%" }}>
          <FormLabel>Hasło *</FormLabel>
          <Input
            type="password"
            placeholder="Wprowadź hasło..."
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </div>
        <div style={{ position: "relative", width: "100%" }}>
          <FormLabel>Powtórz hasło *</FormLabel>
          <Input
            type="password"
            placeholder="Wprowadź hasło ponownie..."
            value={rtpPassword}
            onChange={(e) => setRtpPasswrod(e.target.value)}
          />
        </div>
        <Button type="submit">Aktywuj Konto</Button>
      </Form>
    </FormContainer>
  )
}

export default WithAuth(WithSideNav(Activate), true)
