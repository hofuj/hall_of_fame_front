import React, { useState, useEffect } from "react"
import { RouteComponentProps } from "react-router"

import Fetcher from "../../../common/Fetcher"
import getConfig from "../../../common/config"

import { CenterContainer } from "./AdminProject.style"

import Spinner from "../../Spinner/Spinner"
import ProjectContainer from "../../Project/ProjectContainer"

import ModalTemplate from "../../ModalTemplate/ModalTemplate"

import WithNav from "../../Nav/WithNav"
import WithAuth from "../WithAuth/WithAuth"

import { IProject } from "../../../interfaces/project"

interface QueryParams {
  projectId: string
}

interface IProps extends RouteComponentProps<QueryParams> {}

const AdminProject: React.FC<IProps> = ({ match }) => {
  const [project, setProject] = useState<IProject | null>(null)
  const [isRmModalOpen, setRmModalOpen] = useState<boolean>(false)
  const [isLoading, setIsLoading] = useState<boolean>(false)

  useEffect(() => {
    const fetcher = new Fetcher(getConfig().apiUrl)
    const { params } = match
    ;(async () => {
      setProject(await fetcher.fetchData<IProject>(`/project/${params.projectId}`))
    })()
  }, [match])

  const toggleRmModal = () => {
    setRmModalOpen((oldVal) => !oldVal)
  }

  const removeHandler = async (id: string) => {
    const fetcher = new Fetcher(getConfig().apiUrl)
    setIsLoading(true)
    try {
      await fetcher.deleteData(`/admin/project/${id}`)
      window.location.replace("/admin/projects")
    } catch (e) {
      setIsLoading(false)
    }
  }

  if (project) {
    return (
      <>
        <ModalTemplate
          message="Czy na pewno chcesz usunąć ten projekt?"
          acceptText="Usuń"
          cancelText="Anuluj"
          acceptFunction={removeHandler}
          cancelFunction={toggleRmModal}
          isLoading={isLoading}
          isOpen={isRmModalOpen}
          id={match.params.projectId}
        />

        <ProjectContainer project={project} fromAdmin={true} openRmModal={toggleRmModal} />
      </>
    )
  }

  return (
    <CenterContainer>
      <Spinner />
    </CenterContainer>
  )
}

export default WithAuth(WithNav(AdminProject, true))
