import React, { useState } from "react"

import getConfig from "../../../common/config"
import Fetcher from "../../../common/Fetcher"

import WithSideNav from "../../SideNav/WithSideNav"
import jwtDecode from "jwt-decode"

import { Form, Input, FormContainer, FormLabel, Button } from "./Login.style"

interface SuccessLogin {
  userId: number
  token: string
  isActive: boolean
}

const Login = () => {
  const [login, setLogin] = useState<string>("")
  const [password, setPassword] = useState<string>("")

  const loginHandler = async (e: any) => {
    e.preventDefault()
    const fetcher = new Fetcher(getConfig().apiUrl)
    try {
      const response = await fetcher.postData<SuccessLogin>("/auth/login", {
        username: login,
        password,
      })
      localStorage.setItem("token", response.token)
      const decoded: any = jwtDecode(response.token)
      const route = decoded.isActive ? "/admin/projects" : "/admin/activate"
      window.location.replace(route)
    } catch (e) {
      if (e instanceof Error) console.log(e.message)
    }
  }

  return (
    <FormContainer>
      <Form onSubmit={loginHandler}>
        <div style={{ position: "relative", width: "100%" }}>
          <FormLabel>Login *</FormLabel>
          <Input
            type="text"
            placeholder="Wprowadź login..."
            value={login}
            onChange={(e) => setLogin(e.target.value)}
          />
        </div>
        <div style={{ position: "relative", width: "100%" }}>
          <FormLabel>Hasło *</FormLabel>
          <Input
            type="password"
            placeholder="Wprowadź hasło..."
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </div>
        <Button type="submit">Zaloguj</Button>
      </Form>
    </FormContainer>
  )
}

export default WithSideNav(Login)
