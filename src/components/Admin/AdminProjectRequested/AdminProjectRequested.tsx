import React, { useState, useEffect } from "react"
import { RouteComponentProps } from "react-router"

import Fetcher from "../../../common/Fetcher"
import getConfig from "../../../common/config"

import { CenterContainer } from "./AdminProjectRequested.style"

import Spinner from "../../Spinner/Spinner"
import ProjectContainer from "../../Project/ProjectContainer"

import ModalTemplate from "../../ModalTemplate/ModalTemplate"

import WithNav from "../../Nav/WithNav"
import WithAuth from "../WithAuth/WithAuth"

import { IProject } from "../../../interfaces/project"

interface QueryParams {
  projectId: string
}

interface IProps extends RouteComponentProps<QueryParams> {}

const AdminProjectRequested: React.FC<IProps> = ({ match }) => {
  const [project, setProject] = useState<IProject | null>(null)
  const [isAcceptModalOpen, setAcceptModalOpen] = useState<boolean>(false)
  const [isDenyModalOpen, setDenyModalOpen] = useState<boolean>(false)
  const [isLoading, setIsLoading] = useState<boolean>(false)

  useEffect(() => {
    const fetcher = new Fetcher(getConfig().apiUrl)
    const { params } = match
    ;(async () => {
      setProject(await fetcher.fetchData<IProject>(`/admin/project/requested/${params.projectId}`))
    })()
  }, [match])

  const toggleAcceptModal = () => {
    setAcceptModalOpen((oldVal) => !oldVal)
  }

  const toggleDenyModal = () => {
    setDenyModalOpen((oldVal) => !oldVal)
  }

  const acceptHandler = async (id: string) => {
    const fetcher = new Fetcher(getConfig().apiUrl)
    setIsLoading(true)

    try {
      await fetcher.postData("/admin/project/accept", { id })
      window.location.replace("/admin/requested")
    } catch (e) {
      setIsLoading(false)
    }
  }

  const denyHandler = async (id: string) => {
    const fetcher = new Fetcher(getConfig().apiUrl)
    setIsLoading(true)

    try {
      await fetcher.postData("/admin/project/deny", { id })
      window.location.replace("/admin/requested")
    } catch (e) {
      setIsLoading(false)
    }
  }

  if (project) {
    return (
      <>
        <ModalTemplate
          message="Czy na pewno chcesz akceptować ten projekt?"
          acceptText="Akceptuj"
          cancelText="Anuluj"
          acceptFunction={acceptHandler}
          cancelFunction={toggleAcceptModal}
          isOpen={isAcceptModalOpen}
          isLoading={isLoading}
          id={match.params.projectId}
        />

        <ModalTemplate
          message="Czy na pewno chcesz odrzucić ten projekt?"
          acceptText="Odrzuć"
          cancelText="Anuluj"
          acceptFunction={denyHandler}
          cancelFunction={toggleDenyModal}
          isOpen={isDenyModalOpen}
          isLoading={isLoading}
          id={match.params.projectId}
        />

        <ProjectContainer
          project={project}
          isRequested={true}
          openAcceptModal={toggleAcceptModal}
          openDenyModal={toggleDenyModal}
        />
      </>
    )
  }

  return (
    <CenterContainer>
      <Spinner />
    </CenterContainer>
  )
}

export default WithAuth(WithNav(AdminProjectRequested, true))
