import styled from "styled-components"

export const CenterContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: calc(100vh - 6rem);
  width: 100%;
`
