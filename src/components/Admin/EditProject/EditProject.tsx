import React, { useState, useEffect } from "react"
import { RouteComponentProps } from "react-router"

import Fetcher from "../../../common/Fetcher"
import getConfig from "../../../common/config"

import Spinner from "../../Spinner/Spinner"

import WithSideNav from "../../SideNav/WithSideNav"
import { IProject } from "../../../interfaces/project"

import AddProjectForm from "../../AddProjectForm/AddProjectForm"

import { CenterContainer } from "./EditProject.style"

interface IQueryParams {
  projectId?: string
}

interface IProps extends RouteComponentProps<IQueryParams> {}

const EditProject: React.FC<IProps> = ({ match }) => {
  const [project, setProject] = useState<IProject | null>(null)

  useEffect(() => {
    const fetcher = new Fetcher(getConfig().apiUrl)
    const { params } = match
    ;(async () => {
      const project = await fetcher.fetchData<IProject>(`/project/${params.projectId}`)
      setProject(project)
    })()
  }, [match])

  if (!project) {
    return (
      <CenterContainer>
        <Spinner />
      </CenterContainer>
    )
  }

  const { name, team, codeUrl, websiteUrl, description, category, images } = project

  const mapped = team.members.map((member) => ({
    name: member.name,
    surname: member.surname,
    personalWebsiteUrl: member.personalWebsiteUrl,
  }))
  return (
    <AddProjectForm
      withSideNav={true}
      path={`/admin/project/${match.params.projectId}`}
      dProjectName={name}
      dTeamName={team.name}
      dLiveUrl={websiteUrl}
      dSourceCodeUrl={codeUrl}
      dDescription={description}
      dCategory={category.name}
      dUsers={mapped}
      dImages={images}
      isEdit={true}
    />
  )
}

export default WithSideNav(EditProject)
