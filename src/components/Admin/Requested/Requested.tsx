import React, { useState, useEffect } from "react"
import { RouteComponentProps } from "react-router"

import Fetcher from "../../../common/Fetcher"
import getConfig from "../../../common/config"

import axios from "axios"

import WithSideNav from "../../SideNav/WithSideNav"
import WithAuth from "../WithAuth/WithAuth"

import ProjectsContainer from "../../Projects/ProjectsContainer/ProjectsContainer"
import ModalTemplate from "../../ModalTemplate/ModalTemplate"

import { ISortField } from "../../../interfaces/sort"

interface IQueryParams {
  page?: string
}

interface IProps extends RouteComponentProps<IQueryParams> {
  token: string
}

const Requested: React.FC<IProps> = ({ match, token }) => {
  const [categoryFilter, setCategoryFilter] = useState<string[]>([])
  const [sortField, setSortField] = useState<ISortField>({ asc: false, name: "updatedAt" })
  const [isLoading, setIsLoading] = useState<boolean>(false)
  const [isAcceptModalOpen, setAcceptModalOpen] = useState<boolean>(false)
  const [isDenyModalOpen, setDenyModalOpen] = useState<boolean>(false)

  const [id, setId] = useState<string>("")

  const acceptHandler = async (id: string) => {
    const fetcher = new Fetcher(getConfig().apiUrl)
    setIsLoading(true)

    try {
      await fetcher.postData("/admin/project/accept", { id })
      window.location.replace("/admin/requested")
    } catch (e) {
      setIsLoading(false)
    }
  }

  const denyHandler = async (id: string) => {
    const fetcher = new Fetcher(getConfig().apiUrl)
    setIsLoading(true)

    try {
      await fetcher.postData("/admin/project/deny", { id })
      window.location.replace("/admin/requested")
    } catch (e) {
      setIsLoading(false)
    }
  }

  const toggleAcceptModal = () => {
    setAcceptModalOpen((oldVal) => !oldVal)
  }

  const toggleDenyModal = () => {
    setDenyModalOpen((oldVal) => !oldVal)
  }

  const setPressedId = (id: string) => {
    setId(id)
  }

  return (
    <>
      <ModalTemplate
        message="Czy na pewno chcesz akceptować ten projekt?"
        acceptText="Akceptuj"
        cancelText="Anuluj"
        acceptFunction={acceptHandler}
        cancelFunction={toggleAcceptModal}
        isOpen={isAcceptModalOpen}
        isLoading={isLoading}
        id={id}
      />

      <ModalTemplate
        message="Czy na pewno chcesz odrzucić ten projekt?"
        acceptText="Odrzuć"
        cancelText="Anuluj"
        acceptFunction={denyHandler}
        cancelFunction={toggleDenyModal}
        isOpen={isDenyModalOpen}
        isLoading={isLoading}
        id={id}
      />

      <ProjectsContainer
        params={match.params}
        categoryFilter={categoryFilter}
        sortField={sortField}
        path="/admin/project/requested"
        pagePath="/admin/requested"
        linkTo="/admin/project/requested"
        isFullWidth={true}
        isRequested={true}
        setPressedId={setPressedId}
        toggleAcceptModal={toggleAcceptModal}
        toggleDenyModal={toggleDenyModal}
      />
    </>
  )
}

export default WithAuth(WithSideNav(Requested))
