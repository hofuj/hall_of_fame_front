import React from "react"

import validateToken from "../../../common/utils/validateToken"
import isUserActive from "../../../common/utils/isUserActive"

const WithAuth =
  (Component: React.FC<any>, isActivating?: boolean) =>
  ({ ...props }) => {
    const token = localStorage.getItem("token")

    if (!token || !validateToken(token)) {
      window.location.replace("/admin/login")
      return null
    }

    if (!isActivating && !isUserActive(token)) {
      window.location.replace("/admin/activate")
      return null
    }

    return <Component {...props} token={token} />
  }

export default WithAuth
