import React, { useState } from "react"

import {
  StyledNav,
  LogoContainer,
  Line,
  NavItems,
  NavItem,
  ItemName,
  HamburgerContainer,
} from "./SideNav.style"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faPlus, faList, faListUl, faBars, faTimes } from "@fortawesome/free-solid-svg-icons"

import { Link } from "react-router-dom"

const SideNav = () => {
  const [isVisible, setMenuVisible] = useState<boolean>(false)

  const updateMenuVisibility = () => {
    setMenuVisible((oldVal) => !oldVal)
  }

  return (
    <StyledNav isVisible={isVisible}>
      <LogoContainer>
        <Link to="/">Hall of Fame WMII UJ</Link>
      </LogoContainer>
      <Line />
      <NavItems>
        <NavItem>
          <FontAwesomeIcon icon={faList} />
          <ItemName>
            <Link to="/admin/projects">Projekty</Link>
          </ItemName>
        </NavItem>
        <NavItem>
          <FontAwesomeIcon icon={faListUl} />
          <ItemName>
            <Link to="/admin/requested">Zaproponowane projekty</Link>
          </ItemName>
        </NavItem>
        <NavItem>
          <FontAwesomeIcon icon={faPlus} />
          <ItemName>
            <Link to="/admin/project/new">Dodaj nowy projekt</Link>
          </ItemName>
        </NavItem>
      </NavItems>

      <HamburgerContainer isVisible={isVisible} onClick={updateMenuVisibility}>
        <FontAwesomeIcon icon={isVisible ? faTimes : faBars} />
      </HamburgerContainer>
    </StyledNav>
  )
}

export default SideNav
