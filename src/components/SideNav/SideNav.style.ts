import styled from "styled-components"

interface IVisibleMenu {
  isVisible: boolean
}

export const StyledNav = styled.nav`
  height: 100vh;
  width: 38rem;
  background: #1d1f27;
  position: fixed;
  z-index: 10000;
  color: white;
  display: flex;
  flex-direction: column;
  align-items: center;

  @media (max-width: 1000px) {
    width: 30rem;
  }

  @media (max-width: 700px) {
    transition: all 0.5s;
    left: ${(props: IVisibleMenu) => (props.isVisible ? "0" : "-30rem")};
  }
`

export const LogoContainer = styled.div`
  padding: 3rem 0;
  font-size: 2rem;

  @media (max-width: 1000px) {
    font-size: 1.5rem;
  }

  @media (max-width: 700px) {
    font-size: 1.3rem;
  }
`

export const Line = styled.div`
  width: 100%;
  height: 1px;
  background: white;
`

export const NavItems = styled.ul`
  list-style-type: none;
  margin-top: 1rem;
`

export const NavItem = styled.li`
  margin: 3rem 0;
  font-size: 1.6rem;
  letter-spacing: 1px;

  @media (max-width: 700px) {
    font-size: 1rem;
  }
`

export const ItemName = styled.span`
  display: inline-block;
  margin-left: 1rem;
`

export const MContainer = styled.div`
  margin-left: 38rem;

  @media (max-width: 1000px) {
    margin-left: 30rem;
  }

  @media (max-width: 700px) {
    margin-left: 0;
  }
`

export const HamburgerContainer = styled.div`
  display: none;

  @media (max-width: 700px) {
    background: ${(props: IVisibleMenu) => (props.isVisible ? "#EBEBEB" : "#1d1f27")};
    transition: all 0.6s;
    width: 5rem;
    height: 5rem;
    border-radius: 50%;
    position: fixed;
    top: 2rem;
    left: 4rem;
    display: flex;
    justify-content: center;
    align-items: center;
    color: ${(props: IVisibleMenu) => (props.isVisible ? "#1d1f27" : "#EBEBEB")};
    font-size: 1.6rem;
    cursor: pointer;
    z-index: 100000000;
  }
`
