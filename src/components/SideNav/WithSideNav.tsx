import React from "react"
import SideNav from "./SideNav"

import { MContainer } from "./SideNav.style"

const WithSideNav =
  (Component: React.FC<any>) =>
  ({ ...props }) => {
    return (
      <>
        <SideNav />
        <MContainer>
          <Component {...props} />
        </MContainer>
      </>
    )
  }

export default WithSideNav
