import React, { useState, useEffect } from "react"

import { FormInputSmall } from "../ControlledFormInput/ControlledFormInput.style"

import { FormItemMultiple } from "../AddProjectForm.style"
import ControlledFormInput from "../ControlledFormInput/ControlledFormInput"
import { IUser } from "../../../interfaces/user"

interface IProps {
  index: number
  setUsers: React.Dispatch<React.SetStateAction<IUser[]>>
  dUser?: IUser
}

const NewUserField: React.FC<IProps> = ({ index, setUsers, dUser }) => {
  const [name, setName] = useState<string>(dUser && dUser.name ? dUser.name : "")
  const [surname, setSurname] = useState<string>(dUser && dUser.surname ? dUser.surname : "")
  const [personalWebsiteUrl, setPersonalWebsiteUrl] = useState<string>(
    dUser && dUser.personalWebsiteUrl ? dUser.personalWebsiteUrl : ""
  )

  useEffect(() => {
    setUsers((users) => {
      users[index] = { name, surname, personalWebsiteUrl }
      return users
    })
  }, [name, surname, personalWebsiteUrl])

  return (
    <FormItemMultiple>
      <ControlledFormInput
        small={true}
        withLabel={false}
        type="text"
        placeholder="Imię"
        value={name}
        setValue={setName}
      />
      <ControlledFormInput
        small={true}
        withLabel={false}
        type="text"
        placeholder="Nazwisko"
        value={surname}
        setValue={setSurname}
      />
      <ControlledFormInput
        small={true}
        withLabel={false}
        type="text"
        placeholder="Strona"
        value={personalWebsiteUrl}
        setValue={setPersonalWebsiteUrl}
      />
    </FormItemMultiple>
  )
}

export default NewUserField
