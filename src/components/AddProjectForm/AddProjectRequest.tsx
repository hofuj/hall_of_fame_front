import React from "react"
import AddProjectForm from "./AddProjectForm"

import WithNav from "../Nav/WithNav"

const AddProjectRequest = () => {
  return (
    <>
      <AddProjectForm path="/project" />
    </>
  )
}

export default WithNav(AddProjectRequest)
