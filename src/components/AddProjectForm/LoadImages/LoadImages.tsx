import React, { useCallback, useState } from "react"
import { ImageUrl } from "../AddProjectForm"

import { DropArea, ImagesContainer, Image } from "./LoadImages.style"

interface IProps {
  setImages: (files: any) => void
  removeImage: (name: string) => void
  setImageUrls: (imageUrls: ImageUrl[]) => void
  imageUrls: ImageUrl[]
  images: any[]
}

const LoadImages: React.FC<IProps> = ({
  setImages,
  imageUrls,
  removeImage,
  images,
  setImageUrls,
}) => {
  const changeImages = (newImages: any) => {
    const newImageUrls = Array.from(newImages).map((image: any) => {
      return {
        url: URL.createObjectURL(image),
        name: `image.name${Math.random()}`,
      } as ImageUrl
    })
    const imagesToSet = [...images, ...newImages].slice(0, 5)
    const imageUrlsToSet = [...imageUrls, ...newImageUrls].slice(0, 5)
    setImages(imagesToSet)
    setImageUrls(imageUrlsToSet)
  }

  const onImageChange = (e: any) => {
    const newImages = e.target.files
    changeImages(newImages)
  }

  const onDropImage = (e: any) => {
    const newImages = e.dataTransfer.files

    changeImages(newImages)

    e.preventDefault()
  }

  const onDragOverImage = (e: any) => {
    e.preventDefault()
  }

  if (imageUrls.length >= 5) {
    return (
      <DropArea>
        <label htmlFor="upload-photo">Nie mozna dodac wiecej niz 5 zdjec</label>

        <ImagesContainer>
          {imageUrls.map((imageUrl) => (
            <Image key={imageUrl.name}>
              <img src={imageUrl.url} />
              <span onClick={() => removeImage(imageUrl.name)}>&#10005;</span>
            </Image>
          ))}
        </ImagesContainer>
      </DropArea>
    )
  }

  return (
    <DropArea>
      <label onDrop={onDropImage} onDragOver={onDragOverImage} htmlFor="upload-photo">
        Drop file here
      </label>
      <input
        type="file"
        multiple={true}
        accept="image/*"
        onChange={onImageChange}
        id="upload-photo"
      />
      <ImagesContainer>
        {imageUrls.map((imageUrl) => (
          <Image key={imageUrl.name}>
            <img src={imageUrl.url} />
            <span onClick={() => removeImage(imageUrl.name)}>&#10005;</span>
          </Image>
        ))}
      </ImagesContainer>
    </DropArea>
  )
}

export default LoadImages
