import styled from "styled-components"

export const DropArea = styled.div`
  width: 100%;
  background: #f7f7f7;
  height: 10rem;
  position: relative;

  label {
    cursor: pointer;
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
  }

  #upload-photo {
    opacity: 0;
    position: absolute;
    z-index: -1;
  }
`

export const ImagesContainer = styled.div`
  position: absolute;
  bottom: 5%;
  left: 2%;
  display: flex;
`

export const Image = styled.div`
  width: 3rem;
  height: 3rem;
  margin-right: 1rem;
  position: relative;

  img {
    width: 100%;
    height: 100%;
  }

  span {
    position: absolute;
    color: red;
    font-size: 1.2rem;
    right: 5%;
    cursor: pointer;
  }
`
