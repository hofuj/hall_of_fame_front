import styled from "styled-components"

interface IInput {
  withLabel?: boolean
}

export const FormInput = styled.input`
  width: 100%;
  padding: 1.2rem;
  padding-left: 1.5rem;
  padding-top: ${(props: IInput) => (props.withLabel ? "3rem" : "1.2rem")};
  border: 2px solid #f3f3f3;
  border-radius: 5px;
  font-family: "Roboto", sans-serif;
  font-size: 1.3rem;
  background: #f7f7f7;

  &::placeholder {
    color: #b4b4b4;
  }
`

export const FormInputSmall = styled(FormInput)`
  width: 33%;

  &:not(:first-child) {
    margin-left: 1rem;
  }
`
