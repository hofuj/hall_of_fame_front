import React from "react"

import { FormInput, FormInputSmall } from "./ControlledFormInput.style"

interface IProps {
  small?: boolean
  value: string
  setValue: React.Dispatch<React.SetStateAction<string>>
  placeholder: string
  type: string
  withLabel: boolean
}

const ControlledFormInput: React.FC<IProps> = ({
  small,
  value,
  setValue,
  placeholder,
  type,
  withLabel,
}) => {
  if (small) {
    return (
      <FormInputSmall
        type={type}
        placeholder={placeholder}
        withLabel={withLabel}
        value={value}
        onChange={(e) => setValue(e.target.value)}
      />
    )
  }
  return (
    <FormInput
      type={type}
      placeholder={placeholder}
      withLabel={withLabel}
      value={value}
      onChange={(e) => setValue(e.target.value)}
    />
  )
}

export default ControlledFormInput
