import React from "react"

import WithSideNav from "../SideNav/WithSideNav"
import AddProjectForm from "./AddProjectForm"

const AddProjectAdmin = () => {
  return (
    <>
      <AddProjectForm withSideNav={true} path="/admin/project/" isAdmin={true} />
    </>
  )
}

export default WithSideNav(AddProjectAdmin)
