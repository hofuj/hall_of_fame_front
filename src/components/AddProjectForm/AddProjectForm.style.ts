import styled from "styled-components"

interface IFormItem {
  withSideNav?: boolean
}

export const NewProjectForm = styled.form`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  padding-top: 5rem;
`

export const Title = styled.h2`
  font-size: 4rem;
  text-transform: uppercase;
  margin-bottom: 2rem;
  font-family: "Kanit", sans-serif;

  @media (max-width: 600px) {
    font-size: 3rem;
  }

  @media (max-width: 300px) {
    font-size: 2rem;
  }
`

export const FormItem = styled.div`
  margin: 0.7rem 0;
  width: ${(props: IFormItem) => (props.withSideNav ? "45%" : "35%")};
  display: flex;
  flex-direction: column;
  align-items: center;
  position: relative;

  @media (max-width: 1400px) {
    width: ${(props: IFormItem) => (props.withSideNav ? "55%" : "35%")};
  }

  @media (max-width: 1300px) {
    width: ${(props: IFormItem) => (props.withSideNav ? "55%" : "45%")};
  }

  @media (max-width: 1100px) {
    width: ${(props: IFormItem) => (props.withSideNav ? "65%" : "45%")};
  }

  @media (max-width: 800px) {
    width: ${(props: IFormItem) => (props.withSideNav ? "75%" : "55%")};
  }

  @media (max-width: 600px) {
    width: ${(props: IFormItem) => (props.withSideNav ? "75%" : "70%")};
  }

  @media (max-width: 400px) {
    width: ${(props: IFormItem) => (props.withSideNav ? "75%" : "80%")};
  }
`

export const FormItemMultiple = styled(FormItem)`
  flex-direction: row;
  width: 100%;
`
export const FormLabel = styled.label`
  position: absolute;
  left: 1.8rem;
  top: 1.2rem;
  font-weight: bold;
  font-size: 0.9rem;
`

export const FormTextArea = styled.textarea`
  resize: none;
  width: 100%;
  height: 10rem;
  border: 2px solid #f3f3f3;
  border-radius: 5px;
  font-family: "Roboto", sans-serif;
  font-size: 1.4rem;
  padding: 1.2rem;
  background: #f7f7f7;

  &::placeholder {
    color: #c2c1c1;
  }
`

export const FormButton = styled.button`
  background: #333333;
  border: none;
  border-radius: 5px;
  width: 100%;
  font-family: "Roboto", sans-serif;
  color: white;
  padding: 1.3rem;
  cursor: pointer;
`

export const FormSelect = styled.select`
  // A reset of styles, including removing the default dropdown arrow
  // Additional resets for further consistency
  background-color: transparent;
  margin: 0;
  width: 100%;
  font-family: "Roboto", sans-serif;
  border: 2px solid #f3f3f3;
  border-radius: 5px;
  padding: 1rem;
  font-size: 1.25rem;
  cursor: pointer;
  background-color: #f7f7f7;
  display: grid;

  &::-ms-expand {
    display: none;
    outline: none;
  }
`

export const FormOption = styled.option``

export const ErrorContainer = styled.div`
  position: fixed;
  bottom: 5%;
`
