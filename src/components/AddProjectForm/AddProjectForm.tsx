import React, { useState, useEffect } from "react"
import Fetcher from "../../common/Fetcher"
import getConfig from "../../common/config"
import { ICategory } from "../../interfaces/category"
import { IUser } from "../../interfaces/user"
import { IImage, IProject } from "../../interfaces/project"
import { toast } from "react-toastify"

import Spinner from "../Spinner/Spinner"

import {
  NewProjectForm,
  FormItem,
  Title,
  FormLabel,
  FormButton,
  FormSelect,
  FormOption,
  FormTextArea,
  ErrorContainer,
} from "./AddProjectForm.style"

import ControlledFormInput from "./ControlledFormInput/ControlledFormInput"
import NewUserFields from "./NewUserFields/NewUserFields"
import LoadImages from "./LoadImages/LoadImages"
import { Redirect } from "react-router-dom"

export interface ImageUrl {
  url: string
  name: string
}

interface IProps {
  withSideNav?: boolean
  path: string
  dProjectName?: string
  dTeamName?: string
  dLiveUrl?: string
  dSourceCodeUrl?: string
  dUsers?: IUser[]
  dDescription?: string
  dCategory?: string
  isEdit?: boolean
  dImages?: IImage[]
  isAdmin?: boolean
}

const AddProjectForm: React.FC<IProps> = ({
  withSideNav,
  path,
  dProjectName,
  dTeamName,
  dLiveUrl,
  dSourceCodeUrl,
  dUsers,
  dDescription,
  dCategory,
  isEdit,
  dImages,
  isAdmin,
}) => {
  const [projectName, setProjectName] = useState<string>(dProjectName ? dProjectName : "")
  const [teamName, setTeamName] = useState<string>(dTeamName ? dTeamName : "")
  const [liveUrl, setLiveUrl] = useState<string>(dLiveUrl ? dLiveUrl : "")
  const [sourceCodeUrl, setSourceCodeUrl] = useState<string>(dSourceCodeUrl ? dSourceCodeUrl : "")
  const [users, setUsers] = useState<IUser[]>(dUsers ? dUsers : [])
  const [description, setDescription] = useState<string>(dDescription ? dDescription : "")
  const [category, setCategory] = useState<string>(dCategory ? dCategory : "")
  const [images, setImages] = useState<any[]>([])
  const [imageUrls, setImageUrls] = useState<ImageUrl[]>([])
  const [isLoading, setIsLoading] = useState<boolean>(false)
  const [redirect, setRedirect] = useState<boolean>(false)
  const [projectId, setProjectId] = useState<string>("")

  const [categories, setCategories] = useState<ICategory[]>([])

  const buildFormData = (formData: FormData, data: any, parentKey?: any) => {
    if (data && typeof data === "object" && !(data instanceof Date) && !(data instanceof File)) {
      Object.keys(data).forEach((key) => {
        buildFormData(formData, data[key], parentKey ? `${parentKey}[${key}]` : key)
      })
    } else {
      const value = data === null ? "" : data

      formData.append(parentKey, value)
    }
  }

  const jsonToFormData = (data: any): FormData => {
    const formData = new FormData()

    buildFormData(formData, data)

    return formData
  }

  const submitNewProject = async (e: React.SyntheticEvent) => {
    try {
      e.preventDefault()
      setIsLoading(true)
      const fetcher = new Fetcher(getConfig().apiUrl)

      const imageUrlsToSend = isEdit
        ? imageUrls.map((image) => {
            if (image.url.search("cloudinary.com") !== -1) {
              return image.url
            }
          })
        : null

      const newProjectBody = {
        name: projectName,
        teamName,
        teamMembers: filterUsers(),
        description,
        category,
        codeUrl: sourceCodeUrl,
        websiteUrl: liveUrl,
        imageUrls: imageUrlsToSend,
      }

      const form = jsonToFormData(newProjectBody)
      images.forEach((file) => {
        form.append(file.name, file)
      })

      let response

      if (isEdit) {
        response = await fetcher.putData<IProject>(path, form)
      } else {
        response = await fetcher.postData<IProject>(path, form)
      }
      setProjectId(response.id)
      notify(isAdmin ? "Project was added succesfully" : "Project was requested succesfully")
      setRedirect(true)
    } catch (e: any) {
      setIsLoading(false)
      console.log(e.response.data)
      let message = Array.isArray(e.response.data.message)
        ? e.response.data.message[0]
        : e.response.data.message
      message = message.charAt(0).toUpperCase() + message.slice(1)
      notify(message)
    }
  }

  const removeImage = (name: string) => {
    const newImages = images.filter((image) => image.name !== name)
    const newUrlImages = imageUrls.filter((image) => image.name !== name)

    setImageUrls(newUrlImages)
    setImages(newImages)
  }

  useEffect(() => {
    if (dImages) {
      const initialImageUrls = dImages.map((image, idx) => {
        return {
          url: image.url,
          name: `img${idx}`,
        } as ImageUrl
      })
      setImageUrls(initialImageUrls)
    }

    const fetcher = new Fetcher(getConfig().apiUrl)
    ;(async () => {
      const fetchedCategories = await fetcher.fetchData<ICategory[]>("/category")
      setCategories(fetchedCategories)
      setCategory(fetchedCategories[0].name)
    })()
  }, [])

  const filterUsers = (): IUser[] => {
    const usersCopy = [...users]
    const filtered = usersCopy.filter((user) => user.name !== "")
    return filtered
  }

  const notify = (msg: string) => toast(msg, { position: "bottom-right" })

  if (redirect) {
    return <Redirect to={isAdmin ? `/admin/project/${projectId}` : `/`} />
  }
  return (
    <NewProjectForm onSubmit={submitNewProject}>
      {isLoading ? (
        <Spinner />
      ) : (
        <>
          <Title>{isEdit ? "Edytuj projekt" : "Dodaj nowy projekt"}</Title>
          <FormItem withSideNav={!!withSideNav}>
            <FormLabel>Nazwa projektu *</FormLabel>
            <ControlledFormInput
              withLabel={true}
              type="text"
              placeholder="Wprowadź nazwę projektu..."
              value={projectName}
              setValue={setProjectName}
            />
          </FormItem>
          <FormItem withSideNav={!!withSideNav}>
            <FormLabel>Nazwa zespołu</FormLabel>
            <ControlledFormInput
              withLabel={true}
              type="text"
              placeholder="Wprowadź nazwę zespołu"
              value={teamName}
              setValue={setTeamName}
            />
          </FormItem>

          <NewUserFields users={users} setUsers={setUsers} withSideNav={!!withSideNav} />

          <FormItem withSideNav={!!withSideNav}>
            <FormTextArea
              placeholder="Opis..."
              value={description}
              onChange={(e) => setDescription(e.target.value)}
            />
          </FormItem>

          <FormItem withSideNav={!!withSideNav}>
            <FormSelect
              name="categories"
              id="categories"
              onChange={(e) => setCategory(e.target.value)}
            >
              {categories.map((category: ICategory) => (
                <FormOption key={category.id} value={category.name}>
                  {category.name}
                </FormOption>
              ))}
            </FormSelect>
          </FormItem>

          <FormItem withSideNav={!!withSideNav}>
            <LoadImages
              setImages={setImages}
              imageUrls={imageUrls}
              removeImage={removeImage}
              images={images}
              setImageUrls={setImageUrls}
            />
          </FormItem>

          <FormItem withSideNav={!!withSideNav}>
            <FormLabel>Live URL</FormLabel>
            <ControlledFormInput
              withLabel={true}
              type="text"
              placeholder="Wprowadź Live URL..."
              value={liveUrl}
              setValue={setLiveUrl}
            />
          </FormItem>
          <FormItem withSideNav={!!withSideNav}>
            <FormLabel>Source Code URL</FormLabel>
            <ControlledFormInput
              withLabel={true}
              type="text"
              placeholder="Wprowadź Source code URL..."
              value={sourceCodeUrl}
              setValue={setSourceCodeUrl}
            />
          </FormItem>
          <FormItem withSideNav={!!withSideNav}>
            <FormButton type="submit">{isEdit ? "Edytuj projekt" : "Dodaj projekt"}</FormButton>
          </FormItem>
        </>
      )}
    </NewProjectForm>
  )
}

export default AddProjectForm
