import React, { useState, useEffect } from "react"

import { FormItem } from "../AddProjectForm.style"
import NewUserField from "../NewUserField/NewUserField"

import { IUser } from "../../../interfaces/user"

interface IProps {
  users: IUser[]
  setUsers: React.Dispatch<React.SetStateAction<IUser[]>>
  withSideNav?: boolean
}

const NewUserFields: React.FC<IProps> = ({ users, setUsers, withSideNav }) => {
  const [userFieldsAmount, setUserFieldsAmount] = useState<number>(users.length + 1)

  const addNewField = () => {
    setUserFieldsAmount((fieldsAmount) => fieldsAmount + 1)
    setUsers((users) => [...users, { name: "", surname: "", personalWebsiteUrl: undefined }])
  }

  return (
    <FormItem withSideNav={withSideNav ? true : false}>
      <h3>Dodaj członków zespołu</h3>
      {Array.from(Array(userFieldsAmount).keys()).map((i) => (
        <NewUserField key={i} index={i} setUsers={setUsers} dUser={users[i]} />
      ))}
      <h4 onClick={addNewField}>+ Dodaj kolejną osobę</h4>
    </FormItem>
  )
}

export default NewUserFields
