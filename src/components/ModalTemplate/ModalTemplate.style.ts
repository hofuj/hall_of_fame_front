import styled from "styled-components"

interface IOverlay {
  isOpen: boolean
}

export const FullOverlay = styled.div`
  width: 100vw;
  height: 100vh;
  background: rgba(0, 0, 0, 0.8);
  position: fixed;
  left: 0;
  z-index: 1000000000;
  display: ${(props: IOverlay) => (props.isOpen ? "flex" : "none")};
  justify-content: center;
  align-items: center;
`

export const Modal = styled.div`
  background: white;
  padding: 6rem 20rem;
  border-radius: 1rem;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  font-size: 1.6rem;
`

export const Button = styled.button`
  border: none;
  padding: 0.8rem 1.5rem;
  outline: none;
  border-radius: 5px;
  color: white;
  margin: 0.7rem;
  cursor: pointer;
`

export const RmButton = styled(Button)`
  background: #db3914;
`

export const EdButton = styled(Button)`
  background: #13a4de;
`
