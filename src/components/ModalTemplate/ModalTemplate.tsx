import React from "react"
import Spinner from "../Spinner/Spinner"

import { FullOverlay, Modal, RmButton, EdButton } from "./ModalTemplate.style"

interface IProps {
  message: string
  acceptText: string
  cancelText: string
  acceptFunction: (id: string) => void
  cancelFunction: () => void
  isOpen: boolean
  isLoading?: boolean
  id: string
}

const ModalTemplate: React.FC<IProps> = ({
  message,
  acceptFunction,
  cancelFunction,
  acceptText,
  cancelText,
  isOpen,
  isLoading,
  id,
}) => {
  return (
    <FullOverlay isOpen={isOpen}>
      <Modal>
        {isLoading ? (
          <Spinner />
        ) : (
          <>
            <p>{message}</p>
            <div>
              <RmButton onClick={() => acceptFunction(id)}>{acceptText}</RmButton>
              <EdButton onClick={() => cancelFunction()}>{cancelText}</EdButton>
            </div>
          </>
        )}
      </Modal>
    </FullOverlay>
  )
}

export default ModalTemplate
