import styled from "styled-components"

export const Container = styled.div`
  margin: 5rem 7rem;
  position: relative;

  @media (max-width: 1000px) {
    display: flex;
    flex-direction: column;
    align-items: center;
  }
`

export const Dot = styled.span`
  height: 1rem;
  width: 1rem;
  background-color: black;
  border-radius: 50%;
  display: inline-block;
  margin-right: 0.5rem;
`

export const CenterContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: calc(100vh - 6rem);
  width: 100%;
`

export const AdminButtons = styled.div`
  position: absolute;
  top: 0;
  right: 0;
`

export const AdminButton = styled.button`
  margin: 0 0.5rem;
  padding: 0.9rem 1.5rem;
  background: #1d1f27;
  border: 2px black;
  color: white;
  border-radius: 6px;
  font-size: 1.1rem;
`
