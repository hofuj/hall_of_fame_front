import React, { useState, useEffect } from "react"
import { RouteComponentProps } from "react-router"

import Fetcher from "../../common/Fetcher"
import getConfig from "../../common/config"

import { CenterContainer } from "./Project.style"

import Spinner from "../Spinner/Spinner"

import WithNav from "../Nav/WithNav"

import { IProject } from "../../interfaces/project"

import ProjectContainer from "./ProjectContainer"

interface QueryParams {
  projectId: string
}

interface IProps extends RouteComponentProps<QueryParams> {}

const Project: React.FC<IProps> = ({ match }) => {
  const [project, setProject] = useState<IProject | null>(null)

  useEffect(() => {
    const fetcher = new Fetcher(getConfig().apiUrl)
    const { params } = match
    ;(async () => {
      setProject(await fetcher.fetchData<IProject>(`/project/${params.projectId}`))
    })()
  }, [match])

  if (project) {
    return <ProjectContainer project={project} />
  }

  return (
    <CenterContainer>
      <Spinner />
    </CenterContainer>
  )
}

export default WithNav(Project)
