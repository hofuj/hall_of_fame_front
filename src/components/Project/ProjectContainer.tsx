import React from "react"

import { Container, AdminButtons, AdminButton } from "./Project.style"

import InfoHeader from "./InfoHeader/InfoHeader"
import Description from "./Description/Description"

import { IProject } from "../../interfaces/project"

interface IProps {
  project: IProject
  fromAdmin?: boolean
  isRequested?: boolean
  openRmModal?: () => void
  openAcceptModal?: () => void
  openDenyModal?: () => void
}

const ProjectContainer: React.FC<IProps> = ({
  project,
  fromAdmin,
  isRequested,
  openRmModal,
  openAcceptModal,
  openDenyModal,
}) => {
  const {
    name,
    team,
    images,
    category,
    websiteUrl,
    codeUrl,
    description,
    voteCount,
    currentUserVote,
    id,
  } = project

  const onEditPressed = () => {
    window.location.replace(`/admin/project/edit/${id}`)
  }

  return (
    <Container>
      <InfoHeader
        name={name}
        team={team}
        images={images}
        category={category}
        websiteUrl={websiteUrl}
        codeUrl={codeUrl}
        voteCount={voteCount}
        didVoted={currentUserVote === 1}
        id={id}
        isVotedDisabled={fromAdmin || isRequested}
      />
      <Description desc={description} />

      {fromAdmin && (
        <AdminButtons>
          <AdminButton onClick={openRmModal}>Usuń</AdminButton>
          <AdminButton onClick={onEditPressed}>Edytuj</AdminButton>
        </AdminButtons>
      )}

      {isRequested && (
        <AdminButtons>
          <AdminButton onClick={openAcceptModal}>Akceptuj</AdminButton>
          <AdminButton onClick={openDenyModal}>Odrzuć</AdminButton>
        </AdminButtons>
      )}
    </Container>
  )
}

export default ProjectContainer
