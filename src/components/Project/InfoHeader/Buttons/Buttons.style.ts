import styled from "styled-components"

interface IButtonBG {
  primary?: boolean
}

export const ButtonsContainer = styled.div`
  margin-top: 2rem;
`

export const Button = styled.button`
  color: white;
  background: ${(props: IButtonBG) => (props.primary ? "#5498F2" : "#C11C1C")};
  padding: 1rem 2.2rem;
  border: none;
  border-radius: 7px;
  font-family: "Roboto", sans-serif;
  margin-top: auto;
  cursor: pointer;
`
