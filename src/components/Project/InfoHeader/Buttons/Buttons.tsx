import React from "react"

import { ButtonsContainer, Button } from "./Buttons.style"

interface IProps {
  liveUrl?: string
  codeUrl?: string
}

const Buttons: React.FC<IProps> = ({ liveUrl, codeUrl }) => {
  return (
    <ButtonsContainer>
      <Button>
      {!liveUrl ? <span><s>Live</s></span> : 
          <a target="_blank" rel="noreferrer" href={liveUrl ? liveUrl : "#"}>Live</a>
        }
      </Button>
      <Button primary={true} style={{ marginLeft: "2rem" }}>
        {!codeUrl ? <span><s>Source Code</s></span> : 
          <a target="_blank" rel="noreferrer" href={codeUrl ? codeUrl : "#"}>Source Code</a>
        }
      </Button>
    </ButtonsContainer>
  )
}

export default Buttons
