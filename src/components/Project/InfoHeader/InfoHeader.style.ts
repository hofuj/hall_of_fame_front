import styled from "styled-components"

export const InfoContainer = styled.header`
  display: flex;

  @media (max-width: 1000px) {
    flex-direction: column;
    align-items: center;
  }
`

export const Info = styled.div`
  margin-left: 4rem;
  width: 55%;

  @media (max-width: 1600px) {
    width: 50%;
  }

  @media (max-width: 1400px) {
    width: 45%;
  }

  @media (max-width: 1200px) {
    width: 40%;
  }

  @media (max-width: 1000px) {
    width: 100%;
    align-self: start;
    margin-left: 0;
  }
`

export const Title = styled.h1`
  font-size: 4rem;
  text-transform: uppercase;
  display: flex;
  margin-bottom: 2rem;

  @media (max-width: 1200px) {
    font-size: 3rem;
  }

  @media (max-width: 1000px) {
    display: none;
  }

  @media (max-width: 600px) {
    font-size: 2rem;
  }
`

export const TitleResp = styled(Title)`
  display: none;

  @media (max-width: 1000px) {
    display: flex;
    align-self: start;
  }
`

export const Name = styled.div`
  @media (max-width: 1000px) {
    max-width: 90%;
  }

  @media (max-width: 800px) {
    max-width: 80%;
  }

  @media (max-width: 500px) {
    max-width: 70%;
  }
`
