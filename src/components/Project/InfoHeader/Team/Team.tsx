import React from "react"

import { TeamName } from "./Team.style"

import TeamMembers from "./TeamMembers/TeamMembers"

import { ITeam } from "../../../../interfaces/project"

interface IProps {
  team: ITeam
}

const Team: React.FC<IProps> = ({ team }) => {
  return (
    <>
      <TeamName>{team.name}</TeamName>
      <TeamMembers members={team.members} />
    </>
  )
}

export default Team
