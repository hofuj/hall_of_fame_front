import styled from "styled-components"

export const TeamName = styled.h2`
  font-size: 2rem;
`
