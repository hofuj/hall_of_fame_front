import styled from "styled-components"

export const TeamMembersList = styled.ul`
  list-style-type: none;
`

export const TeamMember = styled.li`
  font-size: 1.1rem;
  letter-spacing: 1px;
  margin-top: 0.75rem;
  display: flex;
  align-items: center;
`
