import React from "react"

import { Dot } from "../../../Project.style"
import { TeamMembersList, TeamMember } from "./TeamMembers.style"

import { ITeamMember } from "../../../../../interfaces/project"

interface IProps {
  members: ITeamMember[]
}

const TeamMembers: React.FC<IProps> = ({ members }) => {
  return (
    <TeamMembersList>
      {members.map((member) => (
        <TeamMember key={member.id}>
          <Dot />
          {member.name} {member.surname}
        </TeamMember>
      ))}
    </TeamMembersList>
  )
}

export default TeamMembers
