import React from "react"
import { ICategory } from "../../../../interfaces/category"

import { Dot } from "../../Project.style"
import { SubjectTitle, SubjectName } from "./Subject.style"

interface IProps {
  category: ICategory
}

const Subject: React.FC<IProps> = ({ category }) => {
  return (
    <>
      <SubjectTitle>Zrealizowany w ramach:</SubjectTitle>
      <SubjectName>
        <Dot />
        {category.name}
      </SubjectName>
    </>
  )
}

export default Subject
