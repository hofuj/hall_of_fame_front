import styled from "styled-components"

export const SubjectTitle = styled.h3`
  margin-top: 2.3rem;
  font-size: 1.7rem;
`

export const SubjectName = styled.p`
  font-size: 1.1rem;
  letter-spacing: 1px;
  margin-top: 0.75rem;
  display: flex;
  align-items: center;
`
