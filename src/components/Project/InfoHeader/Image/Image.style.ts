import styled from "styled-components"

export const ImgContainer = styled.div`
  position: relative;
  max-width: 45%;
  height: 50rem;
  overflow-x: hidden;
  overflow-y: hidden;

  @media (max-width: 1600px) {
    max-width: 50%;
  }

  @media (max-width: 1400px) {
    max-width: 55%;
  }

  @media (max-width: 1200px) {
    max-width: 60%;
  }

  @media (max-width: 1000px) {
    max-width: 100%;
    margin-bottom: 2rem;
    align-self: start;
  }

  @media (max-width: 700px) {
    max-width: 100%;
    height: 40rem;
  }

  @media (max-width: 500px) {
    max-width: 100%;
    height: 35rem;
  }

  @media (max-width: 450px) {
    max-width: 100%;
    height: 30rem;
  }
`

export const Img = styled.img`
  max-width: 100%;
  max-height: 100%;
`
