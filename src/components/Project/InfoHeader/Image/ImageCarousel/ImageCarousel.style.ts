import styled from "styled-components"

export const ImgHidden = styled.img`
  max-width: 100%;
  max-height: 100%;
  position: absolute;
`

export const Arrow = styled.div`
  position: absolute;
  background: rgba(0, 0, 0, 0.1);
  height: 100%;
  z-index: 1000;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  padding: 0.5rem;
`

export const ArrowLeft = styled(Arrow)`
  left: 0;
  /* transform: translate(0, -50%); */
`

export const ArrowRight = styled(Arrow)`
  right: 0;
  /* transform: translate(0, -50%); */
`
