import React, { useState } from "react"
import { IImage } from "../../../../../interfaces/project"

import { ImgHidden, ArrowLeft, ArrowRight } from "./ImageCarousel.style"

import { Img, ImgContainer } from "../Image.style"

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faArrowRight, faArrowLeft } from "@fortawesome/free-solid-svg-icons"

interface IProps {
  srcs: IImage[]
}

const ImageCarousel: React.FC<IProps> = ({ srcs }) => {
  const [index, setIndex] = useState<number>(0)
  const [positionLeft, setPositionLeft] = useState({ left: "-100%" })
  const [positionRight, setPositionRight] = useState({ right: "-100%" })
  const [blocked, setBlocked] = useState<boolean>(false)

  const getPreviousIndex = (index: number) => (index === 0 ? srcs.length - 1 : index - 1)
  const getNextIndex = (index: number) => (index === srcs.length - 1 ? 0 : index + 1)

  const incrementIndex = () => setIndex((oldVal) => (oldVal === srcs.length - 1 ? 0 : oldVal + 1))
  const decrementIndex = () => setIndex((oldVal) => (oldVal === 0 ? srcs.length - 1 : oldVal - 1))

  const moveLeftImage = () => {
    setPositionLeft({ left: "0" })
    decrementIndex()
    setPositionLeft({ left: "-100%" })
  }

  const moveRightImage = () => {
    setPositionRight({ right: "0" })
    incrementIndex()
    setPositionRight({ right: "-100%" })
  }

  return (
    <ImgContainer>
      <ArrowLeft onClick={moveRightImage}>
        <FontAwesomeIcon style={{ fontSize: "1.7rem" }} icon={faArrowLeft} />
      </ArrowLeft>
      <ArrowRight onClick={moveLeftImage}>
        <FontAwesomeIcon style={{ fontSize: "1.7rem" }} icon={faArrowRight} />
      </ArrowRight>
      <ImgHidden style={positionLeft} src={srcs[getPreviousIndex(index)].url} />
      <Img src={srcs[index].url} />
      <ImgHidden style={positionRight} src={srcs[getNextIndex(index)].url} />
    </ImgContainer>
  )
}

export default ImageCarousel
