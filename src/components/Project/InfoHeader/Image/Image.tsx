import React from "react"

import { ImgContainer, Img } from "./Image.style"

import "react-responsive-carousel/lib/styles/carousel.min.css"
import { Carousel } from "react-responsive-carousel"
import { IImage } from "../../../../interfaces/project"

import ImageCarousel from "./ImageCarousel/ImageCarousel"

interface IProps {
  srcs: IImage[]
}

const Image: React.FC<IProps> = ({ srcs }) => {
  if (srcs.length > 1) {
    return <ImageCarousel srcs={srcs} />
  }

  if (srcs.length === 1) {
    return (
      <ImgContainer>
        <Img src={srcs[0].url} />
      </ImgContainer>
    )
  }

  return (
    <ImgContainer>
      <Img src="https://www.trroofingsheets.co.uk/wp-content/uploads/2016/05/default-no-image-1.png" />
    </ImgContainer>
  )
}

export default Image
