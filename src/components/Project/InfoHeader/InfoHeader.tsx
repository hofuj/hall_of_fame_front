import React from "react"

import { InfoContainer, Info, Title, TitleResp, Name } from "./InfoHeader.style"

import { FlexSpacer } from "../../../styles/common/Common.style"

import Image from "./Image/Image"
import Subject from "./Subject/Subject"
import Buttons from "./Buttons/Buttons"
import Team from "./Team/Team"
import { IImage, ITeam } from "../../../interfaces/project"
import { ICategory } from "../../../interfaces/category"

import Vote from "../../Vote/Vote"

interface IProps {
  images: IImage[]
  name: string
  team: ITeam
  category: ICategory
  websiteUrl?: string
  codeUrl?: string
  didVoted: boolean
  voteCount: number
  id: string
  isVotedDisabled?: boolean
}

const InfoHeader: React.FC<IProps> = ({
  images,
  name,
  team,
  category,
  websiteUrl,
  codeUrl,
  id,
  didVoted,
  voteCount,
  isVotedDisabled,
}) => {
  return (
    <InfoContainer>
      <TitleResp>
        <Name>{name}</Name>
        <Vote
          didVoted={didVoted}
          voteCount={voteCount}
          id={id}
          big={true}
          isVotedDisabled={isVotedDisabled}
        />{" "}
      </TitleResp>
      <Image srcs={images} />
      <Info>
        <Title>
          <Name style={{ maxWidth: "75%" }}>{name}</Name>
          <Vote
            didVoted={didVoted}
            voteCount={voteCount}
            id={id}
            big={true}
            isVotedDisabled={isVotedDisabled}
          />{" "}
        </Title>
        <Team team={team} />
        <Subject category={category} />
        <Buttons codeUrl={codeUrl} liveUrl={websiteUrl} />
      </Info>
    </InfoContainer>
  )
}

export default InfoHeader
