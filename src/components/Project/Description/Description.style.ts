import styled from "styled-components"

export const Desc = styled.p`
  margin-top: 2rem;
  font-size: 1.4rem;
  letter-spacing: 1px;
  line-height: 1.6;

  @media (max-width: 1000px) {
    margin-top: 8.5rem;
  }
`
