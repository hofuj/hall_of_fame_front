import React from "react"

import { Desc } from "./Description.style"

import parse from "html-react-parser"

interface IProps {
  desc: string
}

const Description: React.FC<IProps> = ({ desc }) => {
  return <Desc>{parse(desc)}</Desc>
}

export default Description
