import styled from "styled-components"

export const TitleContainer = styled.div`
  display: flex;
  align-items: center;
`

export const ProjectTitle = styled.h2`
  text-transform: uppercase;
  font-size: 1.4rem;

  @media (max-width: 800px) {
    font-size: 1rem;
  }
`

export const ProjectAuthor = styled.h3`
  font-size: 1.2rem;

  @media (max-width: 800px) {
    font-size: 1rem;
  }
`
