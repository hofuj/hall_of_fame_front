import React from "react"

import { TitleContainer, ProjectTitle } from "./Title.style"
import { FlexSpacer } from "../../../../styles/common/Common.style"

import Vote from "../../../Vote/Vote"

import { Link } from "react-router-dom"

interface IProps {
  id: string
  name: string
  didVoted: boolean
  voteCount: number
  linkTo: string
  isVotedDisabled?: boolean
}

const Title: React.FC<IProps> = ({ id, name, didVoted, voteCount, linkTo, isVotedDisabled }) => {
  return (
    <TitleContainer>
      <ProjectTitle>
        <Link
          to={{
            pathname: `${linkTo}/${id}`,
          }}
        >
          {name}
        </Link>
      </ProjectTitle>
      <FlexSpacer />
      <Vote didVoted={didVoted} voteCount={voteCount} id={id} isVotedDisabled={isVotedDisabled} />
    </TitleContainer>
  )
}

export default Title
