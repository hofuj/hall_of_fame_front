import styled from "styled-components"

export const Desc = styled.p`
  font-size: 1.1rem;
  line-height: 1.4;
  display: -webkit-box;
  -webkit-line-clamp: 3;
  -webkit-box-orient: vertical;
  overflow: hidden;

  @media (max-width: 1000px) {
    font-size: 0.9rem;
    line-height: 1.2;
  }

  @media (max-width: 800px) {
    font-size: 0.8rem;
    line-height: 1;
  }
`
