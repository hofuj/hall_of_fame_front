import React from "react"

import { Desc } from "./Description.style"

interface IProps {
  desc: string
}

const Description: React.FC<IProps> = ({ desc }) => {
  return <Desc>{desc}</Desc>
}

export default Description
