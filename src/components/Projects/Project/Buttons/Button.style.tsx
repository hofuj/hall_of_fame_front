import styled from "styled-components"

interface ButtonTheme {
  primary?: boolean
}

export const StyledButton = styled.button`
  padding: 1rem 1.5rem;
  margin: 0.5rem;
  outline: none;
  border-radius: 5px;
  color: white;
  font-size: 0.9rem;
  border: ${(props: ButtonTheme) => (props.primary ? "1px solid #1086B6" : "1px solid #B42D0E")};
  background: ${(props: ButtonTheme) => (props.primary ? "#13A4DE" : "#DB3914")};
  cursor: pointer;
`
