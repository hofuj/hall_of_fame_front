import React from "react"

import { StyledButton } from "./Button.style"

interface IProps {
  primary?: boolean
  clickHandler?: (...args: any[]) => void
}

const Button: React.FC<IProps> = ({ primary, children, clickHandler }) => {
  return (
    <StyledButton onClick={clickHandler} primary={primary ? true : false}>
      {children}
    </StyledButton>
  )
}

export default Button
