import styled from "styled-components"

export const ProjectContainer = styled.div`
  width: 32rem;
  height: 39rem;
  background: white;
  margin: 2.2rem;
  border: 3px solid #e1e1e1;
  border-radius: 1.5rem;
  position: relative;

  @media (max-width: 1000px) {
    width: 28rem;
    height: 35rem;
  }

  @media (max-width: 800px) {
    width: 23rem;
    height: 30rem;
    margin: 1rem;
  }

  @media (max-width: 400px) {
    width: 20rem;
    height: 27rem;
    margin: 0.5rem;
  }
`

export const DescContainer = styled.div`
  margin: 2rem;
`

export const Dots = styled.div`
  max-width: 40em;
  padding: 0;
  overflow-x: hidden;
  list-style: none;
  margin: 0.5rem 0 1rem 0;
  font-size: 2rem;
  color: #d6d6d6;

  @media (max-width: 800px) {
    margin: 0rem 0 1rem 0;
  }

  @media (max-width: 800px) {
    margin: 0rem 0 1rem 0;
  }

  &:after {
    float: left;
    width: 0;
    white-space: nowrap;
    content: ". . . . . . . . . . . . . . . . . . . . " ". . . . . . . . . . . . . . . . . . . . "
      ". . . . . . . . . . . . . . . . . . . . " ". . . . . . . . . . . . . . . . . . . . "
      ". . . . . . . . . . . . . . . . . . . . " ". . . . . . . . . . . . . . . . . . . . "
      ". . . . . . . . . . . . . . . . . . . . ";
  }
`

export const ButtonsContainer = styled.div`
  position: absolute;
  left: 0.5rem;
  top: 0.2rem;
`
