import styled from "styled-components"

export const ImageContainer = styled.div`
  width: 100%;
  height: 60%;
`

export const Img = styled.img`
  width: 100%;
  height: 100%;
  border-top-left-radius: 1.2rem;
  border-top-right-radius: 1.2rem;
  object-fit: cover;
  object-position: top;
`
