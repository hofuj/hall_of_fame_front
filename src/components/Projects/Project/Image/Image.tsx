import React from "react"

import { ImageContainer, Img } from "./Image.style"
import { Link } from "react-router-dom"

interface IProps {
  id: string
  source: string
  linkTo: string
}

const defaultImageSource =
  "https://www.trroofingsheets.co.uk/wp-content/uploads/2016/05/default-no-image-1.png"

const Image: React.FC<IProps> = ({ id, source, linkTo }) => {
  return (
    <ImageContainer>
      <Link
        to={{
          pathname: `${linkTo}/${id}`,
        }}
      >
        <Img src={source || defaultImageSource} />
      </Link>
    </ImageContainer>
  )
}

export default Image
