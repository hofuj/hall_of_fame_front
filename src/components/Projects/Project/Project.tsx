import React from "react"

import { ProjectContainer, DescContainer, Dots, ButtonsContainer } from "./Project.style"

import Button from "./Buttons/Button"

import Title from "./Title/Title"
import Description from "./Description/Description"
import Image from "./Image/Image"

import { IProject } from "../../../interfaces/project"

interface IProps extends IProject {
  isAdmin?: boolean
  toggleRmModal?: () => void
  setPressedId?: (id: string) => void
  linkTo: string
  isRequested?: boolean
  toggleDenyModal?: () => void
  toggleAcceptModal?: () => void
}

const Project: React.FC<IProps> = ({
  id,
  name,
  description,
  images,
  currentUserVote,
  voteCount,
  isAdmin,
  toggleRmModal,
  setPressedId,
  linkTo,
  isRequested,
  toggleDenyModal,
  toggleAcceptModal,
}) => {
  const onRemovePressed = () => {
    if (setPressedId && toggleRmModal) {
      setPressedId(id)
      toggleRmModal()
    }
  }

  const onEditPressed = () => {
    window.location.replace(`/admin/project/edit/${id}`)
  }

  const onAcceptPressed = () => {
    if (setPressedId && toggleAcceptModal) {
      setPressedId(id)
      toggleAcceptModal()
    }
  }

  const onDenyPressed = () => {
    if (setPressedId && toggleDenyModal) {
      setPressedId(id)
      toggleDenyModal()
    }
  }

  return (
    <ProjectContainer>
      <Image id={id} source={images.length > 0 ? images[0].url : ""} linkTo={linkTo} />
      <DescContainer>
        <Title
          name={name}
          id={id}
          didVoted={currentUserVote === 1}
          voteCount={voteCount}
          linkTo={linkTo}
          isVotedDisabled={isAdmin}
        />
        <Dots />
        <Description desc={description} />
      </DescContainer>
      {isAdmin && (
        <ButtonsContainer>
          <Button clickHandler={onRemovePressed}>Usuń</Button>
          <Button primary={true} clickHandler={onEditPressed}>
            Edytuj
          </Button>
        </ButtonsContainer>
      )}

      {isRequested && (
        <ButtonsContainer>
          <Button clickHandler={onAcceptPressed}>Akceptuj</Button>
          <Button primary={true} clickHandler={onDenyPressed}>
            Odrzuć
          </Button>
        </ButtonsContainer>
      )}
    </ProjectContainer>
  )
}

export default Project
