import styled from "styled-components"

interface FilterBarContainerProps {
  isFilterVisible: boolean
}

export const Container = styled.div`
  width: 100%;
  display: flex;
`

export const FilterBarContainer = styled.div`
  @media (max-width: 600px) {
    position: fixed;
    height: 100vh;
    width: 35rem;
    left: ${(props: FilterBarContainerProps) => (props.isFilterVisible ? "0" : "-35rem")};
    top: 6rem;
    z-index: 10000000;
    transition: all 0.5s;
    border-left: 3px solid #1d1f27;
  }
`

export const ShowFilterButton = styled.div`
  display: none;

  @media (max-width: 600px) {
    background: #1d1f27;
    width: 5rem;
    height: 5rem;
    border-radius: 50%;
    position: fixed;
    top: 8rem;
    right: 4rem;
    display: flex;
    justify-content: center;
    align-items: center;
    color: white;
    font-size: 1.3rem;
    cursor: pointer;
    z-index: 100000000;
  }
`
