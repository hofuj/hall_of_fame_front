import React, { useState, useEffect } from "react"

import { FilterContainer, CheckboxContainer, Checkbox, CheckboxLabel } from "../FilterBar.style"

interface IProps {
  title: string
  fields: ISortingField[]
  defaultValueIndex: number
  editSortField: (newValue: any) => void
}

interface ISortingField {
  name: string
  value: any
}

const FilterBy: React.FC<IProps> = ({ title, fields, defaultValueIndex, editSortField }) => {
  const [checkedFieldIndex, setCheckedFieldIndex] = useState<number>(defaultValueIndex)

  const checkboxPressed = (index: number) => {
    setCheckedFieldIndex(index)
  }

  useEffect(() => {
    editSortField(fields[checkedFieldIndex].value)
  }, [checkedFieldIndex])

  return (
    <FilterContainer>
      <h3>{title}</h3>
      {fields.map((field, i) => (
        <CheckboxContainer key={field.name}>
          <Checkbox
            onClick={() => checkboxPressed(i)}
            checked={i === checkedFieldIndex}
            type="checkbox"
          />
          <CheckboxLabel onClick={() => checkboxPressed(i)}>{field.name}</CheckboxLabel>
        </CheckboxContainer>
      ))}
    </FilterContainer>
  )
}

export default FilterBy
