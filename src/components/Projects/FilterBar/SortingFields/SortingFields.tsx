import React from "react"
import { ISortField } from "../../../../interfaces/sort"

import SortingBy from "../SortingBy/SortingBy"

interface IProps {
  changeSortField: (sortField: ISortField) => void
  sortField: ISortField
}

const SortingFields: React.FC<IProps> = ({ changeSortField, sortField }) => {
  const editAscSortField = (newValue: boolean) => {
    const newSortField: ISortField = Object.assign({}, sortField)
    newSortField.asc = newValue
    changeSortField(newSortField)
  }

  const editNameSortField = (newValue: "updatedAt" | "voteCount") => {
    const newSortField: ISortField = Object.assign({}, sortField)
    newSortField.name = newValue
    changeSortField(newSortField)
  }

  return (
    <>
      <SortingBy
        title="SORTUJ"
        defaultValueIndex={0}
        editSortField={editAscSortField}
        fields={[
          {
            name: "Malejąco",
            value: false,
          },
          {
            name: "Rosnąco",
            value: true,
          },
        ]}
      />

      <SortingBy
        title="WEDŁUG"
        defaultValueIndex={0}
        editSortField={editNameSortField}
        fields={[
          {
            name: "Data dodania",
            value: "updatedAt",
          },
          {
            name: "Ilość polubień",
            value: "voteCount",
          },
        ]}
      />
    </>
  )
}

export default SortingFields
