import React, { useEffect, useState } from "react"

import Fetcher from "../../../../common/Fetcher"
import getConfig from "../../../../common/config"
import { ICategory } from "../../../../interfaces/category"

import { FilterContainer, CheckboxContainer, Checkbox, CheckboxLabel } from "../FilterBar.style"
import { MultipleFilterField } from "./FilterSubject.style"

interface IProps {
  changeCategoryFilter: (newCategoryFilter: string[]) => void
}

const FilterSubject: React.FC<IProps> = ({ changeCategoryFilter }) => {
  const [categories, setCategories] = useState<ICategory[]>([])
  const [checkedMap, setCheckedMap] = useState<Record<string, boolean>>({})

  useEffect(() => {
    const fetcher = new Fetcher(getConfig().apiUrl)
    ;(async () => {
      const fetchedCategories = await fetcher.fetchData<ICategory[]>("/category")

      const checkMap: Record<string, boolean> = {}

      fetchedCategories.forEach((category) => {
        checkMap[category.id] = true
      })

      setCategories(fetchedCategories)
      setCheckedMap(checkMap)
    })()
  }, [])

  useEffect(() => {
    changeFilters()
  }, [checkedMap])

  const setAllCheckBoxes = (value: boolean) => {
    const newCheckedMap: Record<string, boolean> = {}
    Object.keys(checkedMap).forEach((categoryId) => {
      newCheckedMap[categoryId] = value
    })

    setCheckedMap(newCheckedMap)
  }

  const checkPressedLabelCheckBox = (categoryId: string) => {
    const newCheckedMap: Record<string, boolean> = Object.assign({}, checkedMap)
    newCheckedMap[categoryId] = !checkedMap[categoryId]
    setCheckedMap(newCheckedMap)
  }

  const changeFilters = () => {
    const catFilter: string[] = []
    Object.keys(checkedMap).forEach((categoryId) => {
      if (checkedMap[categoryId]) {
        catFilter.push(categoryId)
      }
    })

    changeCategoryFilter(catFilter)
  }

  return (
    <FilterContainer>
      <div>
        <MultipleFilterField onClick={() => setAllCheckBoxes(true)}>Wszystkie</MultipleFilterField>
        <MultipleFilterField onClick={() => setAllCheckBoxes(false)} style={{ marginLeft: "10px" }}>
          Żadne
        </MultipleFilterField>
      </div>

      {categories.map((category) => (
        <CheckboxContainer key={category.id}>
          <Checkbox
            onClick={() => checkPressedLabelCheckBox(category.id)}
            type="checkbox"
            id={category.id}
            checked={checkedMap[category.id]}
          />
          <CheckboxLabel onClick={() => checkPressedLabelCheckBox(category.id)}>
            {category.name}
          </CheckboxLabel>
        </CheckboxContainer>
      ))}
    </FilterContainer>
  )
}

export default FilterSubject
