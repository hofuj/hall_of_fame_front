import styled from "styled-components"

export const MultipleFilterField = styled.span`
  color: #7f7f7f;
  cursor: pointer;
`
