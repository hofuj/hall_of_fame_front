import React from "react"

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import {
  Container,
  Form,
  SearchContainer,
  IconLabel,
  SearchInput,
  FixedContainer,
} from "./FilterBar.style"

import FilterSubject from "./FilterSubject/FilterSubject"
import SortingFields from "./SortingFields/SortingFields"

import { faSearch } from "@fortawesome/free-solid-svg-icons"

import { ISortField } from "../../../interfaces/sort"

interface IProps {
  changeCategoryFilter: (newCategoryFilter: string[]) => void
  changeSortField: (sortField: ISortField) => void
  handleSearchChange: (e: any) => void
  searchValue: string
  sortField: ISortField
  isFullWidth?: boolean
}

const FilterBar: React.FC<IProps> = ({
  changeCategoryFilter,
  changeSortField,
  handleSearchChange,
  searchValue,
  sortField,
  isFullWidth,
}) => {
  return (
    <FixedContainer>
      <Container isFullWidth={isFullWidth ? true : false}>
        <Form>
          <SearchContainer>
            <IconLabel>
              <FontAwesomeIcon icon={faSearch} />
            </IconLabel>
            <SearchInput
              type="text"
              placeholder="Wyszukaj..."
              value={searchValue}
              onChange={handleSearchChange}
            />
          </SearchContainer>

          <FilterSubject changeCategoryFilter={changeCategoryFilter} />

          <SortingFields changeSortField={changeSortField} sortField={sortField} />
        </Form>
      </Container>
    </FixedContainer>
  )
}

export default FilterBar
