import styled from "styled-components"

interface IButtonBG {
  primary?: boolean
}

interface IContainer {
  isFullWidth: boolean
}

export const FixedContainer = styled.div`
  width: 35rem;

  @media (max-width: 1100px) {
    width: 30rem;
  }

  @media (max-width: 800px) {
    width: 25rem;
  }
`

export const Container = styled(FixedContainer)`
  height: ${(props: IContainer) => (props.isFullWidth ? "100vh" : "calc(100vh - 5rem)")};
  background: white;
  border-right: 2px solid #1d1f27;
  position: fixed;
`

export const Form = styled.form`
  display: flex;
  flex-direction: column;
  margin: 3rem 4rem;

  @media (max-width: 1100px) {
    margin: 3rem 3rem;
  }

  @media (max-width: 1100px) {
    margin: 3rem 2.5rem;
  }
`

export const SearchContainer = styled.div`
  position: relative;
`

export const IconLabel = styled.label`
  position: absolute;
  transform: translate(-50%, -50%);
  left: 20px;
  top: 50%;
  font-size: 1.4rem;
`

export const SearchInput = styled.input`
  width: 100%;
  padding: 1.2rem;
  padding-left: 4rem;
  border: 2px solid #333333;
  border-radius: 10px;
  font-family: "Roboto", sans-serif;
  font-size: 1.4rem;

  &::placeholder {
    color: #c2c1c1;
  }
`

export const CheckboxContainer = styled.div`
  margin: 1rem 0;
  display: flex;
  align-items: center;
`

export const Checkbox = styled.input``

export const CheckboxLabel = styled.label`
  margin-left: 1rem;
  font-size: 1.4rem;
  letter-spacing: 1px;

  @media (max-width: 1100px) {
    margin-left: 0.8rem;
    font-size: 1.2rem;
    letter-spacing: 0;
  }
`

export const FilterContainer = styled.div`
  margin-top: 3rem;
  justify-self: left;
`

export const ButtonsContainer = styled.div``

export const Button = styled.button`
  color: white;
  background: ${(props: IButtonBG) => (props.primary ? "#5498F2" : "#C11C1C")};
  padding: 1.2rem 2.5rem;
  border: none;
  border-radius: 7px;
  font-family: "Roboto", sans-serif;
  margin-top: auto;
  font-size: 1.2rem;

  @media (max-width: 1100px) {
    padding: 1rem 2rem;
    font-size: 1rem;
  }
`
