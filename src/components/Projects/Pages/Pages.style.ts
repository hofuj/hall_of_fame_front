import styled from "styled-components"

export const PagesContainer = styled.div`
  display: flex;
  justify-content: right;
  margin: 0 8rem 2rem 8rem;
`

export const PageButton = styled.button`
  background: #1d1f27;
  color: white;
  font-family: "Roboto", sans-serif;
  border: none;
  padding: 0.6rem 1.8rem;
  border-radius: 0.6rem;
  cursor: pointer;
`
