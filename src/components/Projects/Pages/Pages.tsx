import React from "react"

import { Link } from "react-router-dom"

import { PagesContainer, PageButton } from "./Pages.style"
import { FlexSpacer } from "../../../styles/common/Common.style"

interface IProps {
  currentPage: number
  isLast: boolean
  pagePath: string
}

const Pages: React.FC<IProps> = ({ currentPage, isLast, pagePath }) => {
  return (
    <PagesContainer>
      {currentPage !== 1 && (
        <Link to={`${pagePath}/${currentPage - 1}`}>
          <PageButton>Prev</PageButton>
        </Link>
      )}
      <FlexSpacer />
      {!isLast && (
        <Link to={`${pagePath}/${currentPage + 1}`}>
          <PageButton>Next</PageButton>
        </Link>
      )}
    </PagesContainer>
  )
}

export default Pages
