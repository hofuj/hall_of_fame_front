import React, { useState } from "react"
import { RouteComponentProps } from "react-router"

import { Container, FilterBarContainer, ShowFilterButton } from "./Projects.style"

import FilterBar from "./FilterBar/FilterBar"
import ProjectsContainer from "./ProjectsContainer/ProjectsContainer"

import WithNav from "../Nav/WithNav"

import { ISortField } from "../../interfaces/sort"

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faFilter } from "@fortawesome/free-solid-svg-icons"

interface IQueryParams {
  page?: string
}

interface IProps extends RouteComponentProps<IQueryParams> {}

const Projects: React.FC<IProps> = ({ match }) => {
  const [categoryFilter, setCategoryFilter] = useState<string[]>([])
  const [sortField, setSortField] = useState<ISortField>({ asc: false, name: "updatedAt" })
  const [searchValue, setSearchValue] = useState<string>("")
  const [isFilterVisible, setFilterVisible] = useState<boolean>(false)

  const changeCategoryFilter = (newCategoryFilter: string[]) => {
    setCategoryFilter(newCategoryFilter)
  }

  const changeSortField = (newSortField: ISortField) => {
    setSortField(newSortField)
  }

  const handleSearchChange = (e: any) => {
    setSearchValue(e.target.value)
  }

  const updateFilterBar = () => {
    setFilterVisible((oldVal) => !oldVal)
  }

  return (
    <Container>
      <FilterBarContainer isFilterVisible={isFilterVisible}>
        <FilterBar
          changeCategoryFilter={changeCategoryFilter}
          changeSortField={changeSortField}
          sortField={sortField}
          handleSearchChange={handleSearchChange}
          searchValue={searchValue}
        />
      </FilterBarContainer>

      <ProjectsContainer
        params={match.params}
        categoryFilter={categoryFilter}
        sortField={sortField}
        path="/project"
        pagePath="/projects"
        linkTo="/project"
        searchValue={searchValue}
      />

      <ShowFilterButton onClick={updateFilterBar}>
        <FontAwesomeIcon icon={faFilter} />
      </ShowFilterButton>
    </Container>
  )
}

export default WithNav(Projects)
