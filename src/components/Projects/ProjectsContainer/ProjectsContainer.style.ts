import styled from "styled-components"

interface ContainerProps {
  isFullWidth?: boolean
}

export const Container = styled.div`
  width: ${(props: ContainerProps) => (props.isFullWidth ? "100%" : "calc(100% - 35rem)")};

  @media (max-width: 1100px) {
    width: ${(props: ContainerProps) => (props.isFullWidth ? "100%" : "calc(100% - 30rem)")};
  }

  @media (max-width: 800px) {
    width: ${(props: ContainerProps) => (props.isFullWidth ? "100%" : "calc(100% - 25rem)")};
  }

  @media (max-width: 600px) {
    width: 100%;
    padding: 1rem;
  }

  @media (max-width: 600px) {
    padding: 0.2rem;
  }
`

export const PContainer = styled.div`
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  position: relative;
`
