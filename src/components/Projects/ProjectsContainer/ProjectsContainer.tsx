import React, { useEffect, useState } from "react"
import { IProject } from "../../../interfaces/project"

import { Container, PContainer } from "./ProjectsContainer.style"

import Project from "../Project/Project"
import Pages from "../Pages/Pages"
import Spinner from "../../Spinner/Spinner"

import Fetcher from "../../../common/Fetcher"
import getConfig from "../../../common/config"

import isNumeric from "../../../common/utils/isNumeric"
import { ISortField } from "../../../interfaces/sort"

interface IQueryParams {
  page?: string
}

interface IProps {
  params: IQueryParams
  categoryFilter: string[]
  sortField: ISortField
  path: string
  pagePath: string
  linkTo: string
  searchValue?: string
  isAdmin?: boolean
  isRequested?: boolean
  toggleRmModal?: () => void
  setPressedId?: (id: string) => void
  isFullWidth?: boolean
  toggleDenyModal?: () => void
  toggleAcceptModal?: () => void
}

const ProjectsContainer: React.FC<IProps> = ({
  params,
  categoryFilter,
  sortField,
  path,
  pagePath,
  isAdmin,
  toggleRmModal,
  setPressedId,
  linkTo,
  searchValue = "",
  isFullWidth,
  isRequested,
  toggleDenyModal,
  toggleAcceptModal,
}) => {
  const [allProjects, setAllProjects] = useState<IProject[]>([])
  const [projects, setProjects] = useState<IProject[] | null>(null)
  const [pageNum, setPageNum] = useState<number>(0)
  const [isLastPage, setIsLastPage] = useState<boolean>(false)

  useEffect(() => {
    const fetcher = new Fetcher(getConfig().apiUrl)
    ;(async () => {
      const fetchedProjects = await fetcher.fetchData<IProject[]>(path)
      setAllProjects(fetchedProjects)
      prepareCurrentPage(fetchedProjects)
    })()
  }, [params])

  const filterProjects = (projects: IProject[]) => {
    return projects.filter(
      (project) =>
        categoryFilter.includes(project.category.id.toString()) &&
        project.name.startsWith(searchValue)
    )
  }

  const sortProjects = (projects: IProject[]): IProject[] => {
    return projects.sort((project: IProject, nextProject: IProject) =>
      sortField.asc
        ? project[sortField.name] > nextProject[sortField.name]
          ? 1
          : -1
        : project[sortField.name] < nextProject[sortField.name]
        ? 1
        : -1
    )
  }

  const prepareProjects = (projects: IProject[], init: boolean) => {
    return init ? sortProjects(projects) : sortProjects(filterProjects(projects))
  }

  useEffect(() => {
    prepareCurrentPage(prepareProjects(allProjects, false))
  }, [categoryFilter, sortField, searchValue])

  const prepareCurrentPage = (projects: IProject[]) => {
    const { page } = params
    const projectsPerPage = 12

    if (!page) {
      // means its first page
      const prepared = prepareProjects(projects, true)
      const projectsForPage = prepared.slice(0, projectsPerPage)
      setProjects(projectsForPage)
      setPageNum(1)
      setIsLastPage(prepared.length <= projectsPerPage)
    } else if (!isNumeric(page)) {
      // redirect to some error page
      console.log("ERRORRRR")
    } else {
      // means its there and its numeric
      const prepared = prepareProjects(projects, true)
      const pageNum = parseInt(page)
      const startIndex = (pageNum - 1) * projectsPerPage
      const projectsForPage = prepared.slice(startIndex, startIndex + projectsPerPage)
      setProjects(projectsForPage)
      setPageNum(pageNum)
      setIsLastPage(prepared.length <= projectsPerPage * pageNum)
    }
  }

  if (!projects) {
    return (
      <Container isFullWidth={isFullWidth}>
        <PContainer style={{ height: "calc(100vh - 6rem)", alignItems: "center" }}>
          <Spinner />
        </PContainer>
      </Container>
    )
  }

  return (
    <Container isFullWidth={isFullWidth}>
      <PContainer>
        {projects.map((project: IProject) => (
          <Project
            linkTo={linkTo}
            isAdmin={isAdmin}
            key={project.id}
            toggleRmModal={toggleRmModal ? toggleRmModal : () => {}}
            setPressedId={setPressedId}
            isRequested={isRequested}
            toggleDenyModal={toggleDenyModal}
            toggleAcceptModal={toggleAcceptModal}
            {...project}
          />
        ))}
      </PContainer>
      <Pages currentPage={pageNum} isLast={isLastPage} pagePath={pagePath} />
    </Container>
  )
}

export default ProjectsContainer
