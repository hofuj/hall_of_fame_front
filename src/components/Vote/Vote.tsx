import React, { useState } from "react"

import Fetcher from "../../common/Fetcher"
import getConfig from "../../common/config"

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faThumbsUp } from "@fortawesome/free-solid-svg-icons"

interface IProps {
  didVoted: boolean
  voteCount: number
  id: string
  big?: boolean
  isVotedDisabled?: boolean
}

const Vote: React.FC<IProps> = ({ didVoted, id, voteCount, big, isVotedDisabled }) => {
  const [voted, setVoted] = useState<boolean>(didVoted)
  const [amount, setAmount] = useState<number>(voteCount)

  const onVotePress = () => {
    const oldVal = voted
    const fetcher = new Fetcher(getConfig().apiUrl)
    setVoted(!oldVal)
    setAmount((oldAmount) => (oldVal ? oldAmount - 1 : oldAmount + 1))
    fetcher.postData(`/vote/${id}`, { value: !oldVal ? 1 : 0 })
  }

  const thumbColor = voted ? "#227809" : "#B0ACAC"
  return (
    <div>
      <span
        style={
          big
            ? { marginLeft: "4rem", fontSize: "2rem" }
            : { marginRight: ".5rem", fontSize: "1.2rem" }
        }
      >
        {amount}
      </span>
      <FontAwesomeIcon
        onClick={isVotedDisabled ? () => null : onVotePress}
        icon={faThumbsUp}
        style={{
          fontSize: big ? "3rem" : "1.4rem",
          cursor: "pointer",
          color: thumbColor,
          marginLeft: big ? ".8rem" : "0",
        }}
      />
    </div>
  )
}

export default Vote
