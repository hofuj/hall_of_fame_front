import styled from "styled-components"

const navHeight = 6
const navWidth = 100

export const FixedContainer = styled.div`
  height: ${navHeight}rem;
  width: ${navWidth}%;
`

export const StyledNav = styled.nav`
  height: ${navHeight}rem;
  width: ${navWidth}%;
  background: #1d1f27;
  position: fixed;
  z-index: 10000;
`

export const LogoContainer = styled.div`
  color: white;
  font-size: 1.3rem;

  @media (max-width: 400px) {
    font-size: 1rem;
  }
`

export const NavContainer = styled.div`
  display: flex;
  height: 100%;
  align-items: center;
  margin: 0 3.5rem;
`

export const NavItems = styled.ul`
  list-style-type: none;
  display: flex;
`

export const NavItem = styled.li`
  color: white;
  margin: 0 1.3rem;
  font-size: 1.3rem;
  cursor: pointer;

  @media (max-width: 400px) {
    font-size: 1rem;
  }
`
