import React from "react"
import {
  StyledNav,
  NavContainer,
  NavItems,
  NavItem,
  LogoContainer,
  FixedContainer,
} from "./Nav.style"
import { FlexSpacer } from "../../styles/common/Common.style"
import { Link } from "react-router-dom"

interface IProps {
  isAdmin: boolean
}

const Nav: React.FC<IProps> = ({ isAdmin }) => {
  return (
    <FixedContainer>
      <StyledNav>
        <NavContainer>
          <LogoContainer>
            <Link to={isAdmin ? "/admin" : "/projects"}>
              {isAdmin ? "Admin Panel" : "Hall of Fame WMII UJ"}
            </Link>
          </LogoContainer>
          <FlexSpacer />
          <NavItems>
            <NavItem>
              <Link to={isAdmin ? "/admin" : "/projects"}>Przeglądaj</Link>
            </NavItem>
            <NavItem>
              <Link to={isAdmin ? "/admin/project/new" : "/project/new"}>
                {isAdmin ? "Dodaj" : "Zaproponuj"} nowy projekt
              </Link>
            </NavItem>
          </NavItems>
        </NavContainer>
      </StyledNav>
    </FixedContainer>
  )
}

export default Nav
