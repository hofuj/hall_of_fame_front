import React from "react"

import Nav from "./Nav"

/* tslint:disable */

const WithNav =
  <T, >(Component: React.FC<T>, isAdmin = false) =>
  (props: T) => {
    return (
      <>
        <Nav isAdmin={isAdmin} />
        <Component {...props} />
      </>
    )
  }

export default WithNav
