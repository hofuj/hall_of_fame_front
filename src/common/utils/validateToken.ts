import jwtDecode from "jwt-decode"

export default (token: string) => {
  const decoded: any = jwtDecode(token)
  const currTime = Date.now() / 1000

  if (decoded.exp > currTime) {
    return true
  }

  return false
}
