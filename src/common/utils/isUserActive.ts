import jwtDecode from "jwt-decode"

export default (token: string) => {
  const decoded: any = jwtDecode(token)

  return decoded.isActive
}
