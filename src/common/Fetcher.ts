import axios from "axios"

class Fetcher {
  proxy: string | undefined

  constructor(proxy: string | undefined) {
    this.proxy = proxy
  }

  fetchData = async <T>(route: string): Promise<T> => {
    try {
      const token = localStorage.getItem("token")
      const config = {
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Methods": "GET,PUT,POST,DELETE,PATCH,OPTIONS",
          Authorization: `Bearer ${token}`,
        },
      }
      const response = await axios.get<T>(`${this.proxy}${route}`, config)
      return response.data
    } catch (err) {
      throw err
    }
  }

  postData = async <T>(route: string, body: any): Promise<T> => {
    try {
      const token = localStorage.getItem("token")
      const config = {
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Methods": "GET,PUT,POST,DELETE,PATCH,OPTIONS",
          Authorization: `Bearer ${token}`,
        },
      }

      const response = await axios.post<T>(`${this.proxy}${route}`, body, config)
      return response.data
    } catch (err) {
      throw err
    }
  }

  putData = async <T>(route: string, body: any): Promise<T> => {
    try {
      const token = localStorage.getItem("token")
      const config = {
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Methods": "GET,PUT,POST,DELETE,PATCH,OPTIONS",
          Authorization: `Bearer ${token}`,
        },
      }

      const response = await axios.put<T>(`${this.proxy}${route}`, body, config)
      return response.data
    } catch (err) {
      throw err
    }
  }

  deleteData = async <T>(route: string): Promise<T> => {
    try {
      const token = localStorage.getItem("token")
      const config = {
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Methods": "GET,PUT,POST,DELETE,PATCH,OPTIONS",
          Authorization: `Bearer ${token}`,
        },
      }

      const response = await axios.delete<T>(`${this.proxy}${route}`, config)
      return response.data
    } catch (err) {
      throw err
    }
  }
}

export default Fetcher
