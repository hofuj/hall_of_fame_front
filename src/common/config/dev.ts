const devConfig = {
  apiUrl: process.env.REACT_APP_API_LINK,
}

export default devConfig
