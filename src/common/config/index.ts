import dev from "./dev"
import production from "./production"

const getConfig = () => {
  switch (process.env.REACT_APP_ENV) {
    case "dev":
      return dev
    case "production":
      return production
    default:
      return dev
  }
}

export default getConfig
