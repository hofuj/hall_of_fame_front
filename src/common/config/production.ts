const productionConfig = {
  apiUrl: process.env.REACT_APP_API_LINK,
}

export default productionConfig
