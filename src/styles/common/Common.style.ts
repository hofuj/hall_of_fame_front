import styled from "styled-components"

interface IContainerProps {
  centered?: boolean
}

export const FlexSpacer = styled.div`
  flex: 1;
`

export const FullDarkContainer = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  z-index: 100000;
  width: 100%;
  height: 100vh;
  background: rgba(0, 0, 0, 0.75);
  display: ${(props: IContainerProps) => (props.centered ? "flex" : "block")};
  justify-content: ${(props: IContainerProps) => (props.centered ? "center" : "")};
  align-items: ${(props: IContainerProps) => (props.centered ? "center" : "")};
`
